const _ = require('lodash');

const user = {
  name:'Imie',
  surname:'Nazwisko',
  allGrades :[
    {
      subjectName:'Name1',
      grades:[5,4,3,5,2],
      weight:3
    },
    {
      subjectName:'Name2',
      grades:[3,3.5,2],
      weight:1
    },
    {
      subjectName:'Name3',
      grades:[4,3,3.5],
      weight:5
    }
  ]
}

function average(username){
  var sum = 0;
  var weights = 0;

  username.allGrades.forEach((item, i) => {
    item.grades.forEach((item2, i) => {
      sum += item2 * item.weight;
      weights += item.weight;
    });
  });

  console.log('Srednia wazona', _.round(sum/weights,2));
}

average(user);

console.log(_.find(user.allGrades,{weight:1}));
